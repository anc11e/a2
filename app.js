var port = 3000

var express = require("express")
var exphbs  = require('express-handlebars')

var app = express()

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');



app.use(express.static(__dirname + '/Public'));

app.get('/', function (req, res) {
    res.render('home');
});



app.use(express.static(__dirname + '/about'));

app.get('/about', function (req, res) {
    res.render('about');
});




app.get('/helloworld', function(req, res){
    res.send("Hello world")
})

app.listen(port)
console.log("app running on port " + port)

